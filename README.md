Tool for parse [Rebrain](https://rebrainme.com/) courses to [Obsidian](https://obsidian.md/) markdown.

### Usage:
Install requirements.
```shell
$ pip3 install -r requirements.txt
```

```shell
$ ./rebrainget.py -u <USER_EMAIL> -p parse
$ ./rebrainget.py parse -h
usage: rebrainget.py parse [-h] [-s] [-d DIRECTORY] [courses [courses ...]]

positional arguments:
  courses               Courses list.

optional arguments:
  -h, --help            show this help message and exit
  -s, --store           Download attachments.
  -d DIRECTORY, --dir DIRECTORY
                        Directory to download attachments.
```
