#!/usr/bin/env python3

import os
from argparse import ArgumentParser, Action
import getpass
import requests
from bs4 import BeautifulSoup
from markdownify import MarkdownConverter
import json
import re


class PasswordPromptAction(Action):
    def __init__(self,
                 option_strings,
                 dest=None,
                 nargs=0,
                 default=None,
                 required=False,
                 type=None,
                 metavar=None,
                 help=None):
        super(PasswordPromptAction, self).__init__(
            option_strings=option_strings,
            dest=dest,
            nargs=nargs,
            default=default,
            required=required,
            metavar=metavar,
            type=type,
            help=help)

    def __call__(self, parser, args, values, option_string=None):
        password = getpass.getpass('Rebrain password: ')
        setattr(args, self.dest, password)


def md(soup: BeautifulSoup, **options) -> str:
    return MarkdownConverter(**options).convert_soup(soup)


def language_callback(el: BeautifulSoup) -> str:
    language = el.next['class'][0].replace('language-', '') if el.next.has_attr('class') else None
    if language == "sh":
        language = "shell"

    return language


def save_attach(dirname: str, attachdir: str, filename: str, url: str, session: requests.Session) -> None:
    try:
        response = session.get(url, stream=True)
        response.raise_for_status()
    except requests.exceptions.HTTPError as error:
        raise SystemExit(error, error.response.text)

    path = os.path.join(os.getcwd(), dirname)
    if not os.path.isdir(path):
        os.mkdir(path)
    path = os.path.join(path, attachdir)
    if not os.path.isdir(path):
        os.mkdir(path)

    with open(os.path.join(path, filename), 'wb') as file:
        for block in response.iter_content(1024):
            if not block:
                break
            file.write(block)


def parse_task(html: str, load: bool, crs_dir: str, attach_dir: str, session: requests.Session) -> None:
    soup = BeautifulSoup(html, 'html.parser')
    task_name = soup.find('div', class_='tasks__main-area')
    filename = ''.join((task_name.find('div', class_='tasks__main-area_title').text.strip()
                        .replace(':', ' -').replace('/', '_'), '.md'))
    body = soup.find('div', class_='tasks__main-area_body')
    discus = soup.findAll('div', class_='task-check__checker-reply')

    if load:
        imgs = body.findAll('img')
        for img in imgs:
            img_name = img['src'].rsplit('/', 1)[1]
            save_attach(crs_dir, attach_dir, img_name, img['src'], session)
            tag = soup.new_tag('obs_img')
            tag.insert(0, '![[' + img_name + ']]')
            img.replaceWith(tag)

    video = str(body.find('iframe'))
    if video != 'None':
        tag = soup.new_tag('taskvideo')
        tag.insert(0, 'TASK-VIDEO-HERE\n')
        body.find('iframe').replaceWith(tag)

    markdown = md(body, code_language_callback=language_callback, escape_underscores=False) \
        .replace('TASK-VIDEO-HERE', video).replace('\n\n\n', '\n\n').replace('\n\n\n', '\n\n')

    lines = markdown.splitlines(True)
    for i in range(len(lines)):
        if 'Полезные ссылки:' in lines[i]:
            lines[i] = lines[i].replace('###', '>[!info]')
            i += 1
            lines[i] = '>' + lines[i]
            i += 1
            while lines[i] != '\n':
                lines[i] = '>' + lines[i]
                i += 1
            break
    markdown = ''.join(lines)

    discus_md = '\n\n### Решение:\n\n'
    for ds in discus:
        discus_md += md(ds, code_language_callback=language_callback, escape_underscores=False) + '\n\n'

    path = os.path.join(os.getcwd(), crs_dir)
    if not os.path.isdir(path):
        os.mkdir(path)

    with open(os.path.join(path, filename), 'w', encoding='UTF-8') as file:
        file.write(markdown + re.sub(r'(\n\s*)+\n', '\n\n', discus_md))


def get_url(session: requests.Session, url: str) -> str:
    try:
        response = session.get(url)
        response.raise_for_status()
    except requests.exceptions.HTTPError as err:
        raise SystemExit(err, err.response.text)

    return response.text


def parse_course(url: str, crs_dir: str, load: bool, attach_dir: str, session: requests.Session) -> None:
    tasks = json.loads(get_url(session, url + crs_dir))
    for module in tasks['data']['taskModules']:
        for task in module['tasks']:
            if task['status'] != 'unavailable':
                task_url = REBRAIN_URL + '{}/task/{}'.format(crs_dir, task['id'])
                page = get_url(session, task_url)
                parse_task(page, load, crs_dir, attach_dir, session)
    if tasks['data']['final_task']:
        task_url = REBRAIN_URL + '{}/task/{}'.format(crs_dir, tasks['data']['final_task'])
        page = get_url(session, task_url)
        parse_task(page, load, crs_dir, attach_dir, session)


def parse_args():
    parser = ArgumentParser(description='Parse Rebrain courses.')
    parser.add_argument('-u', '--user', dest='user', type=str,
                        required=True, help='User e-mail.')
    parser.add_argument('-p', '--pass', action=PasswordPromptAction, dest='password',
                        type=str, required=True, help='User password.')
    subparser = parser.add_subparsers(title='subcommands', description='valid subcommands',
                                      help='description')
    parser_task = subparser.add_parser('parse', help='Parse tasks.')
    parser_task.add_argument('courses', nargs='*', type=int, help='Courses list.')
    parser_task.add_argument('-s', '--store', action='store_true', dest='load',
                             required=False, default=False, help='Download attachments.')
    parser_task.add_argument('-d', '--dir', dest='directory', type=str,
                             required=False, default='_attachments',
                             help='Directory to download attachments.')

    return parser.parse_args()


def auth(base_url: str, login: str, password: str) -> requests.Session:
    auth_url = base_url + 'auth'
    login_payload = {'password': password, 'remember': 'false', 'email': login}
    session = requests.Session()
    try:
        response = session.put(auth_url, data=login_payload)
        response.raise_for_status()
    except requests.exceptions.HTTPError as err:
        raise SystemExit(err, err.response.text)

    return session


if __name__ == '__main__':
    REBRAIN_URL = 'https://lk.rebrainme.com/'
    API_URL = REBRAIN_URL + 'api/v2/'
    ACC_URL = API_URL + 'account'
    COURSE_URL = API_URL + 'course/'

    args = parse_args()

    sess = auth(API_URL, args.user, args.password)
    payed_courses = [course['name'] for course in json.loads(get_url(sess, ACC_URL))['data']['courses']]
    parseble_courses = [course for course in json.loads(get_url(sess, COURSE_URL + 'list'))['data']['courses'] if
                        course['name'] in payed_courses]

    if hasattr(args, 'courses'):
        if args.courses:
            if min(args.courses) < 1 or max(args.courses) > len(parseble_courses):
                raise SystemExit('\nError: Courses range out of courses available.')
            else:
                for course_id in args.courses:
                    parse_course(COURSE_URL, parseble_courses[course_id - 1]['url'], args.load, args.directory, sess)
        else:
            for course in parseble_courses:
                parse_course(COURSE_URL, course['url'], args.load, args.directory, sess)
    else:
        print('\nAvailable courses:\n')
        for i in range(len(parseble_courses)):
            print(' {} : {}\n'.format(i + 1, parseble_courses[i]['name']))
